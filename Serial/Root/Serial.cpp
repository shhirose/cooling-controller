#include "Serial/Serial.h"

Serial::Serial(std::string port, int baudRate) {
  m_port = port;
  m_isSerialOpen = false;
  m_defaultSleepTime = 300000; // in micro-seconds
  m_fd = 0;
  m_baudRate = baudRate;
}

int Serial::Connect() {
  MSG_DEBUG("Going to open " << m_port << ".");

  m_fd = open(m_port.c_str(), O_RDWR);     // デバイスをオープンする

  if (m_fd < 0) {
    MSG_ERROR("Serial device open error. (" << m_fd << ")");
    return m_fd;
  }
  MSG_INFO("Serial device opened. (" << m_fd << ")");

  m_tio.c_cflag += CREAD;               // 受信有効
  m_tio.c_cflag += CLOCAL;              // ローカルライン（モデム制御なし）
  m_tio.c_cflag += CS8;                 // データビット:8bit
  m_tio.c_cflag += 1;                   // ストップビット:1bit
  m_tio.c_cflag += 0;                   // パリティ:None

  MSG_DEBUG("Baud rate is set to " << m_baudRate);

  cfsetispeed(&m_tio, m_baudRate);
  cfsetospeed(&m_tio, m_baudRate);

  cfmakeraw(&m_tio);                    // RAWモード

  tcsetattr(m_fd, TCSANOW, &m_tio);     // デバイスに設定を行う

  ioctl(m_fd, TCSETS, &m_tio);            // ポートの設定を有効にする

  m_isSerialOpen = true;

  return m_fd;
}

int Serial::SendCommand(std::string command, int Tsleep_in_us) {
  if (! m_isSerialOpen) {
    MSG_ERROR("Error: Serial socket is not opened.");
    return -1;
  }

  int commandLength = strlen(command.c_str());
  MSG_DEBUG("sending... : " << command << " (" << commandLength << ")");
  int checker = write(m_fd, command.c_str(), commandLength);
  usleep(Tsleep_in_us);

  return checker;
}

int Serial::SendCommand(std::string command, std::string &received, int Tsleep_in_us) {
  SendCommand(command, Tsleep_in_us);
  int count = 0;
  int countMax = 10;
  int length = 0;
  char buf[64];
  
  MSG_DEBUG("Start to read back...");
  while (count < countMax) {
    length = read(m_fd, buf, sizeof(buf));
    if (length > 0) break;
    count++;
  }
  if (count == countMax) {
    MSG_ERROR("Timed out!");
  }
  MSG_DEBUG("Read data: " << buf);

  return 0;
}

Serial::~Serial() {
  close(m_fd);
  MSG_DEBUG("Serial port closed.");
}
