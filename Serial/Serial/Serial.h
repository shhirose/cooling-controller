#ifndef SERIAL_H
#define SERIAL_H

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>

#include "message.h"

class Serial {

 public:
  Serial(std::string port, int baudRate);
  ~Serial();
  int Connect();
  int SendCommand(std::string command, int Tsleep_in_us = 300000);
  int SendCommand(std::string command, std::string &received, int Tsleep_in_us = 300000);
  
 private:
  std::string m_port;
  int m_fd;
  int m_defaultSleepTime;
  int m_baudRate;
  bool m_isSerialOpen;
  struct termios m_tio;                 // シリアル通信設定

};

#endif
