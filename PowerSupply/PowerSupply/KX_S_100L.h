#ifndef KX_S_100L_H
#define KX_S_100L_H

#include <iostream>
#include <sstream>
#include "message.h"
#include "PowerSupply/PowerSupply.h"

class KX_S_100L : public PowerSupply {

 public:
  KX_S_100L();
  ~KX_S_100L();
  int OpenSerial(std::string port, int address, int baudRate, int sleepTime_in_us);

  void On() override;
  void Off() override;
  void GetStatus() override;
  void SetVoltage(float V) override;
  float GetVoltage() override;

 private:
  int m_address;
  int m_Tsleep;

};

#endif
