#ifndef ZX_S_400LN_H
#define ZX_S_400LN_H

#include <iostream>
#include <sstream>
#include "message.h"
#include "PowerSupply/PowerSupply.h"

class ZX_S_400LN : public PowerSupply {

 public:
  ZX_S_400LN();
  ~ZX_S_400LN();
  int OpenTCP(std::string IP, int port, int address, int sleepTime_in_us);

  void On() override;
  void Off() override;
  void GetStatus() override;
  void SetVoltage(float V) override;
  float GetVoltage() override;

 private:
  int m_address;
  int m_Tsleep;

};

#endif
