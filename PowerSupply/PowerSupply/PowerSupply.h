#ifndef POWERSUPPLY_H
#define POWERSUPPLY_H

#include <iostream>
#include "TCP/TCP.h"
#include "Serial/Serial.h"
#include "message.h"

class PowerSupply {

 public:
  PowerSupply();
  ~PowerSupply();

  int OpenSerial(std::string port, int baudRate);
  void CloseSerial();
  int OpenTCP(std::string IP, int port);
  void CloseTCP();
  int SendCommand(std::string command);
  int SendCommand(std::string command, std::string &received);
  void SetSleepTime(int sleepTime_in_us) {m_Tsleep = sleepTime_in_us;}
  void SetAddress(int address) {m_address = address;}
  int GetAddress() {return m_address;}

  virtual void On() {};
  virtual void Off() {};
  virtual void GetStatus() {};
  virtual void SetVoltage(float V) {};
  virtual void SetCurrent(float I) {};
  virtual void SetVoltageLimit(float V) {};
  virtual void SetCurrentLimit(float V) {};
  virtual float GetVoltage() {};
  virtual float GetCurrent() {};
  virtual float GetVoltageLimit() {};
  virtual float GetCurrentLimit() {};


 private:
  TCP *m_TCP;
  Serial *m_serial;
  
 protected:
  int m_address;
  int m_Tsleep;
  
};

#endif
