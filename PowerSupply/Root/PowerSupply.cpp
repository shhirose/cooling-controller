#include "PowerSupply/PowerSupply.h"

PowerSupply::PowerSupply() {
  m_address = -1;
  m_Tsleep = 1000000;
}

PowerSupply::~PowerSupply() {

}

int PowerSupply::OpenTCP(std::string IP, int port) {
  m_TCP = new TCP(IP, port);
  int ret = m_TCP->Connect();
  MSG_DEBUG("Returned value = " << ret);
}

int PowerSupply::OpenSerial(std::string port, int baudRate) {
  m_serial = new Serial(port, baudRate);
  int ret = m_serial->Connect();
  MSG_DEBUG("Returned value = " << ret);
}

int PowerSupply::SendCommand(std::string command) {
  std::string received_temp = "no need to read";
  return SendCommand(command, received_temp);
}

int PowerSupply::SendCommand(std::string command, std::string &received) {
  int ret = -1;
  if (m_TCP) {
    ret = m_TCP->SendCommand(command, received, m_Tsleep);
  } else if (m_serial) {
    if (received == "no need to read") {
      ret = m_serial->SendCommand(command, m_Tsleep);
    } else {
      ret = m_serial->SendCommand(command, received, m_Tsleep);
    }
  } else {
    MSG_ERROR("Error: no communication device is open!");
    return -1;
  }
  return ret;
}

void PowerSupply::CloseTCP() {
  if (m_TCP) {
    delete m_TCP;
    MSG_DEBUG("TCP device was closed.");
  } else {
    MSG_ERROR("Error: TCP was not opened.");
  }
}

void PowerSupply::CloseSerial() {
  if (m_serial) {
    delete m_serial;
    MSG_DEBUG("Serial device was closed.");
  } else {
    MSG_ERROR("Error: serial device was not opened.");
  }
}
