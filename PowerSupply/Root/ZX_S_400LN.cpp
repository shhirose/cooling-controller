#include "PowerSupply/ZX_S_400LN.h"

ZX_S_400LN::ZX_S_400LN() {

}

ZX_S_400LN::~ZX_S_400LN() {

}

int ZX_S_400LN::OpenTCP(std::string IP, int port, int address, int sleepTime_in_us) {
  PowerSupply::SetAddress(address);
  PowerSupply::SetSleepTime(sleepTime_in_us);
  PowerSupply::OpenTCP(IP, port);
  std::stringstream command1;
  std::stringstream command2;
  command1 << "ADDR " << address << "\r\n";
  PowerSupply::SendCommand(command1.str());
  command2 << "ALM:CLE\r\n";
  PowerSupply::SendCommand(command2.str());
}

// CloseTCP is defined in PowerSupply.h

void ZX_S_400LN::On() {
  PowerSupply::SendCommand("OUTP ON\r\n");
}

void ZX_S_400LN::Off() {
  PowerSupply::SendCommand("OUTP OFF\r\n");
}

void ZX_S_400LN::GetStatus() {
  PowerSupply::SendCommand("OUTP?\r\n");
}

void ZX_S_400LN::SetVoltage(float V) {
  std::stringstream command;
  command << "VOLT " << V << "\r\n";
  PowerSupply::SendCommand(command.str());
}

float ZX_S_400LN::GetVoltage() {
  std::string received = "";
  PowerSupply::SendCommand("VOLT?\r\n", received);
  float V = atof(received.c_str());
  return V;
}
