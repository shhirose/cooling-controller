#include "PowerSupply/KX_S_100L.h"

KX_S_100L::KX_S_100L() {

}

KX_S_100L::~KX_S_100L() {

}

int KX_S_100L::OpenSerial(std::string port, int address, int baudRate, int sleepTime_in_us) {
  PowerSupply::SetAddress(address);
  PowerSupply::SetSleepTime(sleepTime_in_us);
  PowerSupply::OpenSerial(port, baudRate);
  std::stringstream command;
  command << "A" << address << "\r\n";
  PowerSupply::SendCommand(command.str());
}

// CloseSerial is defined in class PowerSupply

void KX_S_100L::On() {
  std::stringstream command;
  command << "A" << PowerSupply::m_address << ",OT1\r\n";
  PowerSupply::SendCommand(command.str());
}

void KX_S_100L::Off() {
  std::stringstream command;
  command << "A" << PowerSupply::m_address << ",OT0\r\n";
  PowerSupply::SendCommand(command.str());
}

void KX_S_100L::GetStatus() {
  std::stringstream command;
  command << "A" << PowerSupply::m_address << ",TK0\r\n";
  std::string received;
  PowerSupply::SendCommand(command.str(), received);
  MSG_INFO(received);
}

void KX_S_100L::SetVoltage(float V) {
  std::stringstream command;
  command << "A" << PowerSupply::m_address << ",OV" << V << "\r\n";
  PowerSupply::SendCommand(command.str());
}

float KX_S_100L::GetVoltage() {
  float V = 0;
  /*
  std::string received = "";
  PowerSupply::SendCommand("VOLT?\r\n", received);
  float V = atof(received.c_str());
  */
  return V;
}
