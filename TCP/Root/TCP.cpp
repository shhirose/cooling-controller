#include "TCP/TCP.h"

TCP::TCP(std::string IP, int port) {
  m_sockfd = -999;
  m_IP = IP;
  m_port = port;
  m_isTCPopen = false;
  m_defaultSleepTime = 300000; // in micro-seconds
}

int TCP::Connect() {
  if ((m_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    MSG_ERROR("Error: socket failed to create.");
  }
  MSG_DEBUG("sockfd: " << m_sockfd << " (" << AF_INET << ", " << SOCK_STREAM << ")");
  usleep(m_defaultSleepTime);

  struct sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(m_port);
  addr.sin_addr.s_addr = inet_addr(m_IP.c_str());

  MSG_DEBUG("Going to connect to : " << m_IP << ":" << m_port << " (" << m_sockfd << ")");

  int ret = connect(m_sockfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
  usleep(m_defaultSleepTime);

  MSG_DEBUG("Connect: return = " << ret);
  if (ret == 0) m_isTCPopen = true;
  return ret;
}

int TCP::SendCommand(std::string command, std::string &received, int Tsleep_in_us) {
  if (! m_isTCPopen) {
    MSG_ERROR("Error: TCP socket is not opened.");
    return -1;
  }
  int commandSize = strlen(command.c_str());
  MSG_DEBUG("commandSize = " << commandSize);
  int checker = send(m_sockfd, command.c_str(), commandSize, 0);
  char receive_str[100];

  MSG_DEBUG("sending... : " << command << " (" << checker << ")");
  usleep(Tsleep_in_us);

  MSG_DEBUG("Start to check received data.");
  if( checker < 0 ) {
    MSG_ERROR("Error in sending the command.");
  } else {
    recv( m_sockfd, receive_str, sizeof(receive_str), 0);
    MSG_DEBUG("received: " << receive_str);
  }
  received = receive_str;
  usleep(Tsleep_in_us);
  return checker;
}

TCP::~TCP() {
  close (m_sockfd);
}
