#ifndef TCP_H
#define TCP_H

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include "message.h"

class TCP {

 public:
  TCP(std::string IP, int port);
  ~TCP();
  int Connect();
  int SendCommand(std::string command, std::string &received, int Tsleep_in_us = 300000);
  
 private:
  int m_sockfd;
  std::string m_IP;
  int m_port;
  int m_defaultSleepTime;
  bool m_isTCPopen;
};

#endif
