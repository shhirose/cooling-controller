#include <iostream>
#include "PowerSupply/ZX_S_400LN.h"
#include "PowerSupply/KX_S_100L.h"
#include "message.h"

int main (int argc, char *argv[]) {

  if (argc != 2) {
    std::cerr << "Usage: ./monitor [time]" << std::endl;
    return -1;
  }

  KX_S_100L *PS = new KX_S_100L();
  PS->OpenSerial("/dev/ttyUSB0", 1, B9600, 3000000);
  PS->On();
  PS->GetStatus();
  sleep(1);
  PS->Off();
  PS->CloseSerial();
  delete PS;


  /*
  ZX_S_400LN *PS = new ZX_S_400LN();
  PS->OpenTCP("192.168.0.3", 50001, 1, 500000l);
  PS->GetStatus();
  PS->On();
  PS->SetVoltage(2);
  float V = PS->GetVoltage();
  MSG_DEBUG("Measured V = " << V);
  sleep(1);
  PS->SetVoltage(0);
  PS->Off();
  PS->CloseTCP();
  delete PS;
  */

  return 0;

}
